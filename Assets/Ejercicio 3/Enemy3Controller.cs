using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3Controller : MonoBehaviour
{
    public float moveSpeed = 3f;
    public float retreatDistance = 5f;
    public float shootInterval = 1f; // changed from 2f to 1f
    public GameObject bulletPrefab;

    private Transform playerTransform;
    private float timeSinceLastShot = 0f;

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        // Check if the player transform is not null
        if (playerTransform != null)
        {
            // Calculate the distance between the player and the enemy
            float distanceToPlayer = Vector2.Distance(transform.position, playerTransform.position);

            // If the distance to the player is less than the retreat distance
            if (distanceToPlayer < retreatDistance)
            {
                // Move away from the player
                transform.position = Vector2.MoveTowards(transform.position, playerTransform.position, -moveSpeed * Time.deltaTime);

                // If the time since the last shot is greater than the shoot interval
                if (Time.time - timeSinceLastShot > shootInterval)
                {
                    // Shoot a boomerang bullet at the player
                    GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
                    Vector2 direction = (playerTransform.position - transform.position).normalized;
                    bullet.GetComponent<Rigidbody2D>().velocity = direction * 10f;
                    timeSinceLastShot = Time.time;
                }
            }
        }
        else
        {
            // Player transform is null, so try to find it again
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
}

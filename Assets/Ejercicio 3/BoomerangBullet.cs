using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomerangBullet : MonoBehaviour
{
    public float returnTime = 2f;
    public float returnSpeed = 5f;

    private bool isReturning = false;
    private Vector3 startingPosition;
    private float returnStartTime;

    void Start()
    {
        startingPosition = transform.position;
        returnStartTime = Time.time + returnTime;
    }

    void Update()
    {
        // If the bullet is returning
        if (isReturning)
        {
            // Calculate the direction vector from the bullet to its starting position
            Vector3 direction = (startingPosition - transform.position).normalized;

            // Move the bullet towards its starting position at the returnSpeed
            transform.position += direction * returnSpeed * Time.deltaTime;

            // If the bullet has reached its starting position
            if (Vector3.Distance(transform.position, startingPosition) < 0.1f)
            {
                // Destroy the bullet
                Destroy(gameObject);
            }
        }
        // If the bullet is still moving towards the player
        else
        {
            // If the current time is greater than the return start time
            if (Time.time >= returnStartTime)
            {
                // Set the isReturning flag to true
                isReturning = true;
            }
        }
    }
}

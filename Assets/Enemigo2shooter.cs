using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo2shooter : MonoBehaviour
{
    private float timebetweenshots;
    public float starttimebetweenshots;

    public GameObject Bala;
    private Transform player;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        timebetweenshots = starttimebetweenshots;
    }

    void Update()
    {
        if (timebetweenshots <= 0)
        {
            Instantiate(Bala, transform.position, Quaternion.identity);
            timebetweenshots = starttimebetweenshots;
        }
        else
        {
            timebetweenshots -= Time.deltaTime;
        }

    }
}

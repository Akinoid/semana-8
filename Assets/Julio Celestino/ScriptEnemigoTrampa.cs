using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptEnemigoTrampa : MonoBehaviour
{
    public GameObject Gobj;
    public float timer;
    public int maxtime;
    public int mediumtime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Activar();
    }

    public void Activar()
    {
        timer += Time.deltaTime;
        if (timer >= maxtime)
        {
            if (timer >= mediumtime)
            {
                Gobj.SetActive(true);
            }
            if (timer <= maxtime)
            {
                Gobj.SetActive(false);
            }
            timer = 0;
        }
    }
}

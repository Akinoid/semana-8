using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy4Controller : MonoBehaviour
{
    public float bulletSpeed = 5f;
    public Transform firePoint;
    public float stopDuration = 2f;
    public int numStops = 5;
    public GameObject bulletPrefab;
    [SerializeField]public float detectionRange;
    public bool canShoot = false;
    public float shootTimer;
    public float shootInterval = 5f;
    private Transform player;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, player.position) <= detectionRange)
        {
            canShoot = true;

        }
        else
        {
            StopCoroutine(Fire());
            canShoot = false;
        }

        if (canShoot)
        {
            shootTimer += Time.deltaTime;
            
            if (shootTimer >= shootInterval)
            {
                StartCoroutine(Fire());
                shootTimer = 0f;
            }
        }
        
    }

    private IEnumerator Fire()
    {

        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
        Vector2 direction = (player.position - transform.position).normalized;
        bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

        StartCoroutine(StopBullet(bullet));

        yield return new WaitForSeconds(2f);


    }

    private IEnumerator StopBullet(GameObject bullet)
    {
        bool shouldMove = true;

        for (int i = 0; i < numStops; i++)
        {
            if (shouldMove)
            {
                yield return new WaitForSeconds(stopDuration);

                if (bullet != null) // Verificar si el objeto de bala a�n existe
                {
                    bullet.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                    yield return new WaitForSeconds(stopDuration);
                    Vector2 direction = (player.position - transform.position).normalized;

                    if (bullet != null) // Verificar nuevamente si el objeto de bala a�n existe
                    {
                        bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;
                    }
                }

            }
            else
            {
                break;
            }
        }

        if (shouldMove)
        {
            yield return new WaitForSeconds(stopDuration);

            bullet.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            
            shouldMove = true;
        }
        else
        {
            Destroy(bullet);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
        }
    }
}
    


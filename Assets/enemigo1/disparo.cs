using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class disparo : MonoBehaviour
{
    public float moveSpeed = 2f; // Velocidad de movimiento del enemigo
    public float distance = 5f; // Distancia m�xima que se mueve el enemigo antes de invertir direcci�n
    public GameObject bulletPrefab; // Prefab de la bala
    public float bulletSpeed = 5f; // Velocidad de la bala
    public float bulletLifetime = 10f; // Tiempo que tarda la bala en destruirse
    public Transform bulletSpawn; // Punto de spawn de la bala
    public Transform player; // Referencia al transform del jugador

    private Rigidbody2D rb;
    private Vector2 direction = Vector2.down; // Direcci�n inicial de la bala

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        InvokeRepeating("Shoot", 1f, 1f); // Llama a la funci�n Shoot cada 1 segundo
    }

    void Update()
    {
        // Obtiene la posici�n actual del jugador
        Vector3 playerPos = player.position;

        // Establece la direcci�n de la bala hacia abajo
        direction = Vector2.down;

        // Mueve el enemigo en la direcci�n actual
        rb.MovePosition(rb.position + direction * moveSpeed * Time.deltaTime);

       
    }

    void Shoot()
    {
        // Instancia la bala en el punto de spawn
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, Quaternion.identity);

        // Establece la velocidad de la bala
        bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

        // Destruye la bala despu�s de un tiempo determinado
        Destroy(bullet, bulletLifetime);
    }
}
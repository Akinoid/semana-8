using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class daño : MonoBehaviour
{
    public float bulletLifetime = 10f; // Tiempo que tarda la bala en destruirse

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Si la bala colisiona con un objeto, lo destruye
        Destroy(other.gameObject);

        // Destruye la bala
        Destroy(gameObject, bulletLifetime);
    }
}


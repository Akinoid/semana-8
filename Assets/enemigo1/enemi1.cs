using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemi1 : MonoBehaviour
{
    public float moveDistance = 2f;
    public float moveSpeed = 1f;
    private bool moveRight = true;
    private float initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        // se guarda posici�n inicial del enemigo
        initialPosition = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        // si el enemigo est� en su posici�n m�s a la izquierda, cambiamos de direcci�n
        if (transform.position.x <= initialPosition - moveDistance)
        {
            moveRight = true;
        }

        // si el enemigo est� en su posici�n m�s a la derecha, cambiamos de direcci�n
        if (transform.position.x >= initialPosition + moveDistance)
        {
            moveRight = false;
        }

        // se mueve enemigo hacia la derecha o hacia la izquierda seg�n la direcci�n actual
        if (moveRight)
        {
            transform.position = new Vector2(transform.position.x + moveSpeed * Time.deltaTime, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(transform.position.x - moveSpeed * Time.deltaTime, transform.position.y);
        }
    }
}
